<?php
/**
 * @file
 * Contains the comment Atom row style plugin.
 */

/**
 * Plugin which formats the comments as an Atom feed element.
 */
class atom_views_plugin_row_comment extends views_plugin_row {
  function render($row) {
    global $base_url;

    // Load the specified comment:
    $comment = _comment_load($row->cid);

    $entry = new stdClass();
    $entry->title = $comment->subject;
    $entry->id = "tag:$base_url,comment-$comment->cid";
    $entry->updated = atom_views_format_date($comment->timestamp);

    // $entry->link = url('node/' . $comment->nid, array('absolute' => TRUE, 'fragment' => 'comment-' . $comment->cid));

    $item->elements[] = array(
      'key' => 'author',
      'value' => atom_views_format_author($comment),
    );

    $item->elements[] = array(
      'key' => 'content',
      'attributes' => array('type' => 'html'),
      'value' => check_markup($comment->comment, $comment->format, FALSE),
    );

    return theme($this->theme_functions(), $this->view, $this->options, $item);
  }
}
