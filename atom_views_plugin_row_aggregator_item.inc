<?php
/**
 * @file
 * Contains the node Atom row style plugin.
 */

/**
 * Plugin which performs a node_view on the resulting object
 * and formats it as an Atom feed item.
 */
class atom_views_plugin_row_aggregator_item extends views_plugin_row {
  function render($row) {

    $sql =  "SELECT ai.iid, ai.fid, ai.title, ai.link, ai.author, ai.description, ";
    $sql .= "ai.timestamp, ai.guid, ai.link AS link, ";
    $sql .= "af.title as feed_title, af.link AS feed_link, af.image as feed_image, ";
    $sql .= "FROM {aggregator_item} ai LEFT JOIN {aggregator_feed} af ON ai.fid = af.fid ";
    $sql .= "WHERE ai.iid = %d";

    $item = db_fetch_object(db_query($sql, $row->iid));

    $entry = new stdClass();
    $entry->id = $item->guid;
    $entry->updated = atom_views_format_date($item->timestamp);
    $entry->title = check_plain($item->title);

    $entry->elements = array();
    
    // We should really create a full 'source' record for this, but Drupal doesn't
    // give us much information to work with. Maybe for the next version.
    $entry->elements[] = array(
      'key' => 'author',
      'value' => array(
        'name' => $item->author,
      ),
    );
    
    $entry->elements[] = array(
      'key' => 'summary',
      'value' => $item->description,
      'attributes' => array('type' => 'html'),
    );
    
    $entry->elements[] = array(
      'key' => 'link',
      'attributes' => array(
        'href' => $item->link,
        'rel' => 'alternate'
      ),
    );
    
    $entry->elements[] = array(
      'key' => 'link',
      'attributes' => array(
        'href' => $item->feed_link,
        'rel' => 'via'
      ),
    );

    drupal_alter('atom_entry', $entry, 'aggregator_item', $item);
    return theme($this->theme_functions(), $this->view, $this->options, $entry);
  }
}

