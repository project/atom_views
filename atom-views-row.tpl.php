<?php
/**
 * @file atom-views-row.tpl.php
 * Default template for individual Atom feed items.
 *
 * This template only explicitly prints the required Atom entry elements. All
 * other elements are added to the $entry_elements collection, then rendered to
 * XML text before being passed to the template.
 *
 * - $title: the url for the more link
 * - $id: The unique ID of the element (often, but not always, the entry's unaliased
 *        drupal url.)
 * - $updated: The date this entry was last modified.
 *
 * - $entry_elements: Pre-formatted additional elements added to the entry by other
 *     modules.
 *
 * - $view: The raw View object used to generate the feed.
 * - $row: The raw column data for this entry retrieved from the database.
 *
 * @ingroup views_templates
 */
?>
  <entry>
    <title><?php print $title; ?><title>
    <id><?php print $id; ?></id>
    <updated><?php print $updated; ?></updated>
    <?php print $entry_elements; ?>
  </entry>
