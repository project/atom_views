<?php
/**
 * @file atom-views-feed.tpl.php
 * Default template for feed displays that use the Atom style.
 *
 * - $title: The title of the feed
 * - $subtitle: The optional subtitle of the feed
 * - $drupal_version: The current version of Drupal, used to output the Generator link.
 * - $langcode: The two-letter code of the language the content is displayed in.
 *
 * - $id: The unaliased URI of the current feed
 * - $link: The (potentially aliased) URI of the current feed
 * - $alternate_link: The (optional) URI of the current feed's HTML version.
 *
 * - $feed_elements: Pre-formatted additional elements added to the feed by other
 *     modules.
 * - $entries: An array of individually rendered feed entries.
 *
 * - $view: The raw View object used to generate the feed.
 * - $rows: The raw column data retrieved from the database.
 *
 * @ingroup views_templates
 */
?>
<?php print "<?xml"; ?> version="1.0" encoding="utf-8" <?php print "?>"; ?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title type="text"><?php print $title; ?></title>
  <id><?php print $id; ?></id>
  <updated><?php print $updated; ?></updated>
  <link rel="self" type="application/atom+xml" href="<?php print $link; ?>"/>
  <?php print $feed_elements; ?>
  <?php foreach ($entries as $entry): ?>
  <?php print $entry; ?>
  <?php endforeach; ?>
</feed>
