<?php
/**
 * @file atom.views.inc
 * Built in plugins for Views output handling.
 *
 */

/**
 * Implementation of hook_views_plugins
 */
function atom_views_views_plugins() {
  $path = drupal_get_path('module', 'atom_views');
  return array(
    'style' => array(
      'atom' => array(
        'title' => t('Atom feed'),
        'help' => t('Generates an Atom feed from a view.'),
        'handler' => 'atom_views_plugin_style',
        'path' => $path,
        'theme' => 'atom_views_feed',
        'theme path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'feed',
        'help topic' => 'style-atom',
      ),
    ),
    'row' => array(
      'node_atom' => array(
        'title' => t('Node (Atom format)'),
        'help' => t('Display the node with standard node entry.'),
        'handler' => 'atom_views_plugin_row_node',
        'path' => $path,
        'theme' => 'atom_views_row',
        'theme path' => $path,
        'base' => array('node'), // only works with 'node' as base.
        'uses options' => TRUE,
        'type' => 'feed',
        'help topic' => 'style-node-atom',
      ),
      'comment_atom' => array(
        'title' => t('Comment (Atom format)'),
        'help' => t('Display the comment as an Atom feed entry.'),
        'handler' => 'atom_views_plugin_row_comment',
        'path' => $path,
        'theme' => 'atom_views_row',
        'theme path' => $path,
        'base' => array('comments'), // only works with 'comment' as base.
        'type' => 'feed',
        'help topic' => 'style-comment-atom',
      ),
      'aggregator_item_atom' => array(
        'title' => t('Agregator item (Atom format)'),
        'help' => t('Display the aggregator item as an Atom feed entry.'),
        'handler' => 'atom_views_plugin_row_aggregator_item',
        'path' => $path,
        'theme' => 'atom_views_row',
        'theme path' => $path,
        'base' => array('aggregator_item'), // only works with 'comment' as base.
        'type' => 'feed',
        'help topic' => 'style-aggregator-item-atom',
      ),
    ),

  );
}


/**
 * Preprocess an Atom feed
 */
function template_preprocess_atom_views_feed(&$vars) {
  global $base_url;

  $view     = &$vars['view'];
  $options  = &$vars['options'];

  $style    = &$view->style_plugin;

  if ($view->display_handler->get_option('sitename_title')) {
    $title = variable_get('site_name', 'Drupal');
    if ($slogan = variable_get('site_slogan', '')) {
      $title .= ' - ' . $slogan;
    }
  }
  else {
    $title = $view->get_title();
  }
  $vars['title'] = check_plain($title);

  $vars['id'] = check_plain($title);

  // The self-link is a required element for Atom feeds. We'll generated it
  // here instead of sticking it in the elements property.
  $link = $view->display_handler->get_path();
  $url_options = array('absolute' => TRUE);
  if (!empty($view->exposed_raw_input)) {
    $url_options['query'] = $view->exposed_raw_input;
  }
  $vars['link'] = check_url(url($link, $url_options));

  $vars['feed_elements'] = format_xml_elements($style->feed_elements);
  $vars['entries'] = $style->entries;

  drupal_set_header('Content-Type: application/atom+xml; charset=utf-8');
}


/**
 * Default theme function for all Atom rows.
 *
 * This is pretty slim as the majority of the data is contained in the item_elements
 * key, and rendered straight to XML by form_xml_elements().
 */
function template_preprocess_atom_views_row(&$vars) {
  $view     = &$vars['view'];
  $options  = &$vars['options'];
  $entry     = &$vars['row'];

  $vars['title'] = check_plain($entry->title);
  $vars['id'] = check_plain($entry->id);
  $vars['updated'] = check_plain($entry->updated);
  $vars['link'] = check_url($entry->link);

  $vars['entry_elements'] = empty($entry->elements) ? '' : format_xml_elements($entry->elements);
}


function atom_views_format_date($timestamp) {
  $tz = date("O", $timestamp);
  return date("Y-m-d", $timestamp) .'T'. date("H:i:s", $timestamp) . substr($tz, 0, 3) .':'. substr($tz, 3, 2);
}

// A lot like theme_username, but it spits out an XML element definition array.
function atom_views_format_author($object) {
  $element = array();

  if ($object->uid) {
    $account = user_load($object->uid);
    $element['name'] = check_plain($account->name);
    if (user_access('access user profiles')) {
      $element['uri'] = url('user/' . $account->uid, array('absolute' => TRUE));
    }
  }
  else if ($object->name) {
    $element['name'] = check_plain($object->name);
    if (!empty($object->homepage)) {
      $element['uri'] = check_url($object->homepage);
    }
  }
  else {
    $element['name'] = variable_get('anonymous', t('Anonymous'));
  }

  return $element;
}
