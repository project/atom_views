<?php
/**
 * @file
 * Contains the node Atom row style plugin.
 */

/**
 * Plugin which performs a node_view on the resulting object
 * and formats it as an Atom feed item.
 */
class atom_views_plugin_row_node extends views_plugin_row {
  function option_definition() {
    $options = parent::option_definition();

    $options['item_length'] = array('default' => 'default');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['item_length'] = array(
      '#type' => 'select',
      '#title' => t('Display type'),
      '#options' => array(
        'content' => t('Full content'),
        'summary' => t('Summary only (Teaser)'),
      ),
      '#default_value' => $this->options['item_length'],
    );
  }

  function render($row) {
    global $base_url;
    
    $item_length = $this->options['item_length'];
    $teaser = ($item_length == 'summary') ? TRUE : FALSE;

    // Load the specified node and buld its content.
    $node = node_load($row->nid);
    $node->build_mode = 'atom';

    // Filter and prepare node teaser
    if (node_hook($node, 'view')) {
      $node = node_invoke($node, 'view', $teaser, FALSE);
    }
    else {
      $node = node_prepare($node, $teaser);
    }

    // Allow modules to change $node->teaser before viewing.
    node_invoke_nodeapi($node, 'view', $teaser, FALSE);

    // Set the proper node part, then unset unused $node part so that a bad
    // theme can not open a security hole.
    $content = drupal_render($node->content);
    if ($teaser) {
      $node->teaser = $content;
      unset($node->body);
    }
    else {
      $node->body = $content;
      unset($node->teaser);
    }
  
    // Allow modules to modify the fully-built node.
    node_invoke_nodeapi($node, 'alter', $teaser, FALSE);

    // Now we start populating the Atom entry itself.
    $entry = new stdClass();
    $entry->title = $node->title;
    $entry->id = "tag:$base_url,node-$node->nid";
    $entry->updated = atom_views_format_date($node->changed);

    // Might as well stick the originally-published date in there while we're at it.
    $entry->elements['published'] = atom_views_format_date($node->created);

    // Prepare the 'author' chunk.
    if ($author = atom_views_format_author($node)) {
      $entry->elements[] = array(
        'key' => 'author',
        'value' => $author,
      );
    }

    // Always add a basic link back to the item itself.
    $entry->elements[] = array(
      'key' => 'link',
      'attributes' => array(
        'href' => url('node/' . $node->nid, array('absolute' => TRUE)),
      ),
    );


    // Populate the entry content.
    switch ($item_length) {
      case 'summary':
        $entry->elements[] = array(
          'key' => 'summary',
          'attributes' => array('type' => 'html'),
          'value' => $node->teaser,
        );
        break;
      case 'content':
        $entry->elements[] = array(
          'key' => 'content',
          'attributes' => array('type' => 'html'),
          'value' => $node->body,
        );
        break;
    }

    drupal_alter('atom_entry', $entry, 'node', $node);
    return theme($this->theme_functions(), $this->view, $this->options, $entry);
  }
}
